(function($,ko){
	function Article(author, title, createdDate, img, post, post_id){
		var self = this;
		var month=[];
		month[0]="Jan";
		month[1]="Feb";
		month[2]="Mar";
		month[3]="Apr";
		month[4]="May";
		month[5]="Jun";
		month[6]="Jul";
		month[7]="Aug";
		month[8]="Sep";
		month[9]="Oct";
		month[10]="Nov";
		month[11]="Dec";
	
		self.author = ko.observable(author);
		self.title = ko.observable(title);
		self.createdDate = ko.observable(createdDate);
		self.day = createdDate.getDate();
		self.month = month[createdDate.getMonth()];
		self.year = createdDate.getFullYear();
		self.short_caption = post;
		self.profile_img = img;
		self.link = "/blog/"+ post_id;
		self.post = ko.observable(post.substring(0,200));
	
		console.log(self);
	}

	function BlogViewModel(){
		var self = this;
		self.articles = ko.observableArray();
		console.log('creating blogview');
		// methods
		self.addArticle = function(article){
			self.articles.push(new Article(article.author, article.title, article.createdDate));
		};

		self.removeArticle = function(article){
			self.article.remove(article);
		};
			
		$.getJSON("/blogs", function(body){
			console.log('mapping articles');
			$.map(body.rows, function(blog){
				console.log(blog);
				self.articles.push( 
					new Article(blog.value.author, 
						blog.value.title, 
						new Date(blog.key[1]),
						'https://codemonstr.iriscouch.com:6984/blog/' + blog.value._id +'/user.png',
						blog.value.post,
						blog.value._id
					)
				);
			});
		});
	}

ko.applyBindings(new BlogViewModel());
}(window.jQuery,window.ko));