(function($){
	var changeMenuState = function(){
		var siblings = $(this).siblings();
	
		siblings.each(function(idx){
			$(this).removeClass('active');
		});
	
		$(this).addClass('active');
	};

	$(function(){
			$("#menu li").click(changeMenuState);
	});

}(window.jQuery));
