new TWTR.Widget({
  version: 2,
  type: 'profile',
  rpp: 4,
  interval: 30000,
  width: 250,
  height: 300,
  theme: {
    shell: {
      background: '#474447',
      color: '#e858a0'
    },
    tweets: {
      background: '#ffffff',
      color: '#c7c7c7',
      links: '#e858a0'
    }
  },
  features: {
    scrollbar: true,
    loop: false,
    live: true,
    behavior: 'all'
  }
}).render().setUser('cdemonstr').start();