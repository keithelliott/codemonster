var config = require('./config')()
,		nano = require('nano')("https://"+ config.username+":"+config.password+"@"+config.url)
,		blog = nano.use('blog')
,		fs = require('fs');

function addComment(commenter, comment_img, comment, comment_date, likes, replies){
	return {
		"commenter" : commenter,
		"comment_img" : comment_img,
		"comment" : comment,
		"comment_date" : comment_date,
		"likes" : likes,
		"replies": replies
	};
}

function insertArticle(article){
	blog.insert({
		"author": article.author,
		"title": article.title,
		"type": "post",
		"post": article.post,
		"create_dt": article.date,
		"comments": article.comments
	}, function insertImageAndPostDoc(e,b,h){
	if(!e){
		fs.readFile('./public/images/keith.png', function(err, d){
			if(!err){
				console.log(b);
				blog.attachment.insert(b.id,"user.png",d,'image/png',{
				"rev": b.rev
				},function(){
					fs.readFile('./public/posts/samplepost.md', 'utf-8', function(err,d){
					if(!e){
						blog.get(b.id,function(e,b){
							b.post = d;
							blog.insert(b,function(e,b,h){
								if(e){
									console.log(e);
								}
								else{
									console.log('successful in updating doc');
								}});
							});
						}
					});
				});
			}
			else{
			console.log(err);
			}
		});
	}
});
}




function populateViews(callback){
	var article = {};
	article.author = "Keith Elliott";
	article.title = "My First Blog";
	article.date = new Date();
	article.comments = [];
	
	article.comments.push(addComment("Grace Frank", "", "I like this blog", new Date("7/13/2012"), 1, 0));
	article.comments.push(addComment("Wil Wang", "", "I like this blog", new Date("7/12/2012"), 0, 0));
	article.comments.push(addComment("Tim Savery", "", "I like this blog", new Date("7/14/2012"), 2, 0));

	console.log('populating view');
	var i = 0,
	total = 10;
	for(i; i < total; i++){
		console.log('adding article ' + i );
		article.title = "Blog" + i;
		insertArticle(article);
	}
}

var testData = {
	populateViews: populateViews
};

module.exports = testData;
