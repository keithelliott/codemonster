var	config = require('./config')(),
fs = require('fs'),
nano = require('nano')("https://"+ config.username+":"+config.password+"@"+config.url),
db_name = 'blog',
blog,
testData = require("../lib/testData"),
Converter = require('../public/js/Markdown.Converter').Converter,
conv = new Converter(),
td = testData;

function getBlogs(callback){
	console.log('get view');
		var b = nano.use('blog');
	  b.view('blogger', 'by_author_and_date', function(err, body) {
		if (!err) {
			console.log('found '+ body.total_rows + ' rows');
			console.log('sending data to view');
			body.rows.forEach(function(row){
				console.log(row.value);
				row.value.post = conv.makeHtml(row.value.post);
			});
			callback.json(body);
		}
		else{
			console.log(err);
		}
	});
}

function create_views(callback){
	blog.insert({
		"views":
		{
			"by_author_and_date":
			{
				"map": function(doc){
					if(doc.type === 'post'){
						if(doc.author && doc.create_dt){
							emit([doc.author, doc.create_dt],doc);
						}
					}
				}
			},
			"by_date":
			{
				"map": function(doc){if(doc.type === 'post'){if(doc.author && doc.create_dt){emit(doc.create_dt,{'author' : doc.author,'title'	 : doc.title,"post": doc.post,"img":doc.img});}}}
			}
		}
	},"_design/blogger", function(error, body, header){
		if(error){
			console.log('views exists');
			getBlogs(callback);
		}
		else{
			console.log('successfully added view');
			td.populateViews(callback);
		}
	});
}

function create_db(dbname, callback){
	console.log('creating db');
	nano.db.create(dbname, function(e,b,h){
		blog = nano.use(dbname);
		if(e){
			console.log('db exists');
			getBlogs(callback);
		}
		else{
			console.log('successful in creating db');
			// create the views
			console.log('creating views');
			create_views(callback);
		}
	});
}

function get_db(dbname){
	// create db and views
	return nano.use(dbname, function(error,body,header){
		if(error){
			// no db, so create one
			console.log("db not there... creating");
			create_db(dbname);
		}
		else{
			console.log('using existing db');
		}
	});
}


function find(req, res){
	var id = req.params.id;
	var b = nano.use('blog');
	var compareTime = new Date();
	var diffMs,diffHrs, diffMins, diffDays, i, cnt;
	b.get(id, function(err,body){
		if(err){
			console.log(err);
		}
		else{
			body.post = conv.makeHtml(body.post);
		/*	cnt = body.comments.length;
			console.log('comments: ' + cnt);
			for(i = 0; i < cnt; i++){
				if(body.comments[i].comment_date !== ''){
					console.log('comparing dates... today: ' + compareTime.toDateString() + ' and comment date: ' 
					+ new Date(body.comments[i].comment_date).toDateString());
					diffMs = compareTime - new Date(body.comments[i].comment_date);
					diffDays = Math.round(diffMs / 86400000); // days
				  diffHrs = Math.round((diffMs % 86400000) / 3600000); // hours
				  diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
				
					if(diffDays < 1 && diffHrs < 1){
						if(diffMins < 1){
							body.comments[i].comment_date = diffMins + " min ago";
						}
						else if(diffMins < 60){
							body.comments[i].comment_date = diffMins + " mins ago";
						}
					}
					else{
						body.comments[i].comment_date = new Date(body.comments[i].comment_date).toDateString();
					}
				}
			}
			*/
			res.render('post', {post: body});
		}
	});
}

function like(req,res){
	var blog_id = req.params.blog_id;
	console.log(blog_id);
	var comment_id = req.params.comment_id;
	console.log(comment_id);
	var b = nano.use('blog');
	b.get(blog_id, function(err,body){
		if(err){
			console.log(err);
		}
		else{
			console.dir(body);
			var i, cnt = body.comments.length;
			for(i=0; i < cnt; i++){
				if(body.comments[i].comment_date === comment_id){
					body.comments[i].likes = body.comments[i].likes + 1;	
					console.log('updating comment ' + comment_id);
				}
			}
			
			b.insert(body, function(err,body){
				if(err){
					console.log(err);
				}
				else{
					console.log('returning post');
					res.redirect('/blog/'+ blog_id);
				}
			});
		}
	});
}

var blogProvider = {
	findall: function(callback){
		console.log('findall called');
		var blog  = nano.use('blog');
			if(!blog){
				create_db(db_name, callback);
			}
			else{
				console.log('db exists');
				getBlogs(callback);
			}
	},
	find: find,
	like: like,
	getBlogs: getBlogs
};

module.exports = blogProvider;