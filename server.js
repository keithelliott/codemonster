var express = require('express');
var app = express.createServer();
var blogProvider = require('./lib/blogProvider');
var bp = blogProvider;


app.configure(function () {
	app.use(app.router);
	app.use(express.static(__dirname + '/public'));
	app.set('view engine', 'jade');
	app.set('views', __dirname + '/views');
	app.use(express.methodOverride());
	app.use(express.bodyParser());
	app.use(express.logger());
});

app.get('/', function (req, res) {
	console.log('get called');
	res.render('blog', {"view": "blog"});
});

app.get('/blogs', function (req, res) {
	console.log('get blog() called');
	bp.findall(res);
});

app.get('/blog/:id', function (req, res) {
	console.log('retreiving blog');
	bp.find(req, res);
});

app.post('/like/:blog_id/:comment_id', function(req,res){
	console.log('like blog id' + req.params.blog_id);
	bp.like(req,res);
});

app.post('/comment/:id', function (req, res) {
	console.log('adding comment to post');
});

app.post('/blog', function (req, res) {
	console.log('post blog entry');
});

app.listen(3000);